# Puppet

Provides scripts for setting up a symfony web server

## Production

Copy these files on the server somewhere and then run:

    sudo puppet apply manifests/init.pp --modulepath=modules --environment production
    
## Troubleshooting

* This may fail on bootstrapCSS, ignore this (unless you specifically need something there that is failing), it should still be running for you
* Another failure could return something like ```Error: /Stage[main]/Main/Node[default]/Varnish::Vcl[/etc/varnish/default.vcl]/Exec[vcl_reload]: Failed to call refresh``` which can also be ignored.  It appears because where are purposefully setting VARNISH_ADMIN_LISTEN_ADDRESS to an empty string

## Documentation

Information on using puppet is now held on the development wiki: [wiki.alpha.org](http://wiki.alpha.org/index.php/Puppet)