node default {

    if $::environment == 'production' {

        group {'webadmins':
            ensure => present,
        }

        exec {"adduser":
            command => "/usr/sbin/useradd webadmin -g webadmins",
            require => Group['webadmins'],
        }

    }

    yumrepo { 'epel':
        descr => 'epel repo',
        mirrorlist => 'http://mirrors.fedoraproject.org/mirrorlist?repo=epel-6&arch=$basearch',
        enabled => 1,
        gpgcheck => 1,
        gpgkey => 'https://fedoraproject.org/static/0608B895.txt',
    }

    yumrepo {'remi':
        descr => 'Les RPM de remi pour Enterprise Linux $releasever $basearch',
        mirrorlist => 'http://rpms.famillecollet.com/enterprise/$releasever/remi/mirror',
        enabled => 1,
        gpgcheck => 1,
        gpgkey => 'http://rpms.famillecollet.com/RPM-GPG-KEY-remi',
    }

    if $::environment == 'production' {
        $packages = ['php', 'php-common', 'php-pdo', 'php-mysqlnd', 'php-mbstring', 'php-mcrypt', 'php-cli', 'php-intl', 'php-gd', 'php-pecl-memcached-2.1.0', 'php-pear', 'git', 'php-pecl-apc', 'vim-enhanced']
    }
    else {
        $packages = ['php', 'php-common', 'php-pdo', 'php-mysqlnd', 'php-mbstring', 'php-mcrypt', 'php-cli', 'php-intl', 'php-gd', 'php-pecl-memcached-2.1.0', 'php-pecl-xdebug', 'php-pear', 'git', 'php-pecl-apc', 'vim-enhanced', 'bash-completion', 'gitflow']
    }

    package {$packages:
        ensure => present,
        before => File['/etc/php.ini'],
        require => Yumrepo['remi'],
    }

    package {'memcached':
        ensure => present,
        require => Yumrepo['remi'],
    }

    package {'ruby':
        ensure => present,
    }

    #this is used for wkhtmltopdf (tmpc)
    #!! running Xvfb on vagrant box shows this: The XKEYBOARD keymap compiler (xkbcomp) reports: > Internal error:   Could not resolve keysym XF86AudioMicMute Errors from xkbcomp are not fatal to the X server (SO BEWARE IF EVENTUALLY USING THIS TO PROVISION LIVE SERVER. TEST BY SEEING IF PDFS ARE GENERATED AND SENT TO SUPPORT COUPLE (tmpc) WHEN BOTH COUPLES COMPLETE SURVEY). may also need libxrender1
    package { 'xorg-x11-server-Xvfb':
        ensure => present,
    }

    file { '/etc/php.ini':
        ensure => present,
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => template('php/php.ini'),
        require => Package['php'],
    }

    #symlink to project files in home directory (so obvious where to go for contractors)
    file { '/home/vagrant/project':
       ensure => 'link',
       target => '/vagrant',
    }


    if $::environment == 'development' {

        #base .bashrc file with some helpful aliases (won't replace if any new changes when re-provisioning)
        file { '/home/vagrant/.bashrc':
            replace => "no",
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0644',
            content => template('user/.bashrc')
        }        

        #base .bash_profile file with some helpful aliases (won't replace if any new changes when re-provisioning)
        file { '/home/vagrant/.bash_profile':
            replace => "no",
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0644',
            content => template('user/.bash_profile')
        }
        
        #base .gitconfig file with some helpful aliases (won't replace if any new changes when re-provisioning)           
        file { '/home/vagrant/.gitconfig':
            replace => "no",
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0644',
            content => template('user/.gitconfig')
        }              

        file { '/home/vagrant/.git-completion.bash':
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0644',
            content => template('user/.git-completion.bash')
        }        

        file { '/etc/php.d/xdebug.ini':
            ensure => present,
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => template('php/xdebug.ini'),
            require => Package['php-pecl-xdebug'],
        }

        file { '/home/vagrant/.ssh/config':
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0400',
            content => template('keys/config'),
        }

        file { '/home/vagrant/.ssh/bitbucket':
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0400',
            content => template('keys/bitbucket'),
        }

        file { '/etc/httpd/conf/httpd.conf':
            ensure => present,
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => template('httpd/httpd_dev.conf'),
            require => Package['httpd'],
            notify => Service['httpd'],
        }

        file { '/etc/bash_completion.d/symfony2-autocomplete.bash':
            ensure => present,
            owner => 'vagrant',
            group => 'vagrant',
            mode => '0644',
            require => Package['bash-completion'],
            content => template('extras/symfony2-autocomplete.bash'),
        }     

        package {'httpd':
            ensure => present,
        }

        service {'httpd':
            ensure => running,
            enable => true,
            require => Package['httpd'],
            subscribe => File['/etc/php.ini'],
        }

        file { '/etc/hosts':
            ensure => present,
            content => template('hosts/hosts'),
        }

    }
    else {

        file { "/var/www/symfony":
            ensure => "directory",
            owner  => "webadmin",
            group  => "webadmins",
            mode   => '0755',
        }

        file { "/home/webadmin/.ssh":
            ensure => "directory",
            owner  => "webadmin",
            group  => "webadmins",
            mode   => '0700',
        }

        file { '/home/webadmin/.ssh/config':
            ensure => present,
            owner => 'webadmin',
            group => 'webadmins',
            mode => '0400',
            content => template('keys/config'),
        }

        file { '/home/webadmin/.ssh/bitbucket':
            ensure => present,
            owner => 'webadmin',
            group => 'webadmins',
            mode => '0400',
            content => template('keys/bitbucket'),
        }

        file { '/etc/httpd/conf/httpd.conf':
            ensure => present,
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => template('httpd/httpd_prod.conf'),
            require => Package['httpd'],
            notify => Service['httpd'],
        }

        package {'httpd':
            ensure => present,
        }

        service {'httpd':
            ensure => running,
            enable => true,
            require => Package['httpd'],
            subscribe => File['/etc/php.ini'],
        }

        class { 'composer': }

    }

    #this is for tmpc for creating pdfs from html5 content 
    file { '/usr/local/bin/wkhtmltopdf':
        ensure => present,
        owner => 'apache',
        group => 'apache',
        mode => '777',
        content => template('wkhtmltopdf/wkhtmltopdf'),
        require => Package['xorg-x11-server-Xvfb']
    }    
    file { '/usr/local/bin/wkhtmltopdf.sh':
        ensure => present,
        owner => 'apache',
        group => 'apache',
        mode => '777',
        content => template('wkhtmltopdf/wkhtmltopdf.sh')
    }


    file { "/var/log/symfony":
        ensure => "directory",
        owner  => "root",
        group  => "root",
        mode   => 777,
    }

    include ::iptables

    if $::environment == 'development' {

        package {'mysql-server':
            ensure => 'present',
        }

        package {'nano':
            ensure => 'present',
        }

        service {'mysqld':
            ensure => running,
            enable => true,
            require => Package['mysql-server'],
        }

        service {'memcached':
            ensure => running,
            enable => true,
            require => Package['memcached'],
        }

        class { 'composer': }
        ~>
        exec {'symfony_setup':
            command => '/var/www/symfony/install.sh',
            path    => ['/usr/local/bin/', '/bin/', '/usr/bin/'],
            user => vagrant,
            group => vagrant,
            logoutput => false,
            returns => [0, 1],
        }

    }

    class { 'nodejs': }

    if ! defined(Package['rubygems']) {
        package { ["rubygems"]:
            ensure => 'installed'
        }
    }

    if ! defined(Package['ruby-devel']) {
        package { ['ruby-devel']:
            ensure => 'installed',
            before => Package['compass']
        }
    }

    if ! defined(Package['sass']) {
        package { ['sass']:
            ensure => 'installed',
            provider => 'gem',
            require => Package['rubygems']
        }
    }

    if ! defined(Package['compass']) {
        package { ['compass']:
            ensure => 'installed',
            provider => 'gem',
            require => Package['rubygems']
        }
    }
    
    if ! defined(Package['bootstrap-sass']) {
        package { ['bootstrap-sass']:
            ensure => 'installed',
            provider => 'gem',
            require => Package['rubygems']
        }
    }
    
    if ! defined(Package['bourbon']) {
        package { ['bourbon']:
            ensure => 'installed',
            provider => 'gem',
            require => Package['sass']
        }
    }

    if ! defined(Package['neat']) {
        package { ['neat']:
            ensure => 'installed',
            provider => 'gem',
            require => Package['bourbon']
        }
    }

    if ! defined(Package['less']) {
        package { 'less':
          ensure   => '1.7.0',
          provider => 'npm',
          require => [
              Class['::nodejs']
          ]
        }
    }

    if ! defined(Package['uglify-js']) {
        package { 'uglify-js':
          ensure   => '2.4.13',
          provider => 'npm',
          require => [
              Class['::nodejs']
          ]
        }
    }

    if ! defined(Package['uglifycss']) {
        package { 'uglifycss':
          ensure   => '0.0.8',
          provider => 'npm',
          require => [
              Class['::nodejs']
          ]
        }
    }

    # Enable and configure the varnish module from: https://forge.puppetlabs.com/bashtoni/varnish
    class { 'varnish':
      secret => '6565bd1c-b6d1-4ba3-99bc-3c7a41ffd94f',
      varnish_version => '3.0',
      admin_port => '6082',
      admin_listen => '',
      listen => '',
      listen_port => '80',
      storage_size => '1G'
    }
    varnish::vcl { '/etc/varnish/default.vcl':
      content => template('htb_custom/myvcl.erb')
    }
}
