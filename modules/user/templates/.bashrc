# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions

### Symfony2 ###
alias ass="app/console assets:install; app/console assetic:dump;"
alias ass2="app/console assets:install --symlink; app/console assetic:dump;"
alias sf="app/console"

### Go up the number of directories
### http://serverfault.com/questions/3743/what-useful-things-can-one-add-to-ones-bashrc?page=1&tab=votes#tab-top
up(){
  local d=""
  limit=$1
  for ((i=1 ; i <= limit ; i++))
    do
      d=$d/..
    done
  d=$(echo $d | sed 's/^\///')
  if [ -z "$d" ]; then
    d=..
  fi
  cd $d
}

##this is for symfony2 autocompletion (app/console asse (tab) will show options). it installs symfony2-autocomplete.bash into /etc/bash_completion.d
if [ -e ~/symfony2-autocomplete.bash ]; then
    . ~/symfony2-autocomplete.bash
fi
